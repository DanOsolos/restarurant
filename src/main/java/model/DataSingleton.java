package model;

import db.CategoryRepository;
import db.MenuRepository;
import db.ProductRepository;
import db.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class DataSingleton {
    public MenuRepository menuRepository = new MenuRepository();
    public CategoryRepository categoryRepository = new CategoryRepository();
    public ProductRepository productRepository = new ProductRepository();
    public UserRepository userRepository = new UserRepository();

    /**
     * Singleton pattern
     */
    private static DataSingleton _instance;

    public static DataSingleton getInstance() {
        if (_instance == null) {
            _instance = new DataSingleton();
        }
        return _instance;
    }

    private List<Menu> menuList = new ArrayList<>();

    /**
     * Populates the transient values of the menu as well
     */
    public void loadMenus() {
        menuList = menuRepository.findAll();
        for (Menu menu : menuList) {
            List<Category> categoriesList = DataSingleton.getInstance().categoryRepository.findByMenuId(menu.getMenuId());
            menu.setCategoryList(categoriesList);
            for (Category category : categoriesList) {
                List<Product> productList = DataSingleton.getInstance().productRepository.findByCategoryId(category.getCategoryId());
                category.setProducts(productList);
            }
        }
    }

    public List<Menu> getMenuList() {
        return menuList;
    }
}
