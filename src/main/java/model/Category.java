package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table (name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "categoryId")
    private int categoryId;

    @Column (name = "name")
    private String name;

    @Column (name = "menuId")
    private int menuId;

    @Transient
    private List<Product> products = new ArrayList<>();

    public Category() {
    }

    public Category(String name, int menuId) {
        this.name = name;
        this.menuId = menuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void addItemsToMenu(Product product) {
        if (this.products != null) {
            this.products.add(product);
        }
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", menuId=" + menuId +
                '}';
    }
}
