package db;

import model.Menu;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import java.util.List;

public class MenuRepository {

    public void save(Menu menu) {
        Transaction transaction = null;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(menu);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Menu> findAll() {
        List<Menu> allMenu;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            String string = "from Menu";
            Query query = session.createQuery(string);
            allMenu = query.list();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return allMenu;
    }

    public Menu findById(int id) {
        Menu menu = null;
        try {
            SessionFactory sessionFactory = DBHelper.getSessionFactory();
            Session session = sessionFactory.openSession();
            menu = session.find(Menu.class, id);
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return menu;
    }
}
