package db;

import model.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import java.util.List;

public class ProductRepository {
    public List<Product> findAll() {
        List<Product> allProduct;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            String string = "from Product";
            Query query = session.createQuery(string);
            allProduct = query.list();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return allProduct;
    }

    public Product findById(int id) {
        Product product = null;
        try {
            SessionFactory sessionFactory = DBHelper.getSessionFactory();
            Session session = sessionFactory.openSession();
            product = session.find(Product.class, id);
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return product;
    }

    public List<Product> findByCategoryId(int categoryId) {
        List<Product> allProduct;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            String string = "from Product p WHERE p.categoryId = :categoryId";
            Query query = session.createQuery(string);
            query.setParameter("categoryId", categoryId);
            allProduct = query.list();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return allProduct;
    }

    public void save(Product product) {
        Transaction transaction = null;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(product);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void delete(Product product) {
        Transaction transaction = null;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(product);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
