package db;

import model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import java.util.List;

public class UserRepository {

    public List<User> findAll() {
        List<User> allUsers;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            String string = "from User";
            Query query = session.createQuery(string);
            allUsers = query.list();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return allUsers;
    }

    public void save(User user) {
        Transaction transaction = null;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void delete(User user) {
        Transaction transaction = null;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(user);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
