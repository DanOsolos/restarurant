package db;

import model.Category;
import model.Menu;
import model.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class CategoryRepository {

    /**
     * @return returns a list of all categories
     */
    public List<Category> findAll() {
        List<Category> allCategories;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            String string = "from Category";
            Query query = session.createQuery(string);
            allCategories = query.list();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return allCategories;
    }

    /**
     * @param id the id of the category we want to return
     * @return returns a category if found or null otherwise
     */
    public Category findById(int id) {
        Category category = null;
        try {
            SessionFactory sessionFactory = DBHelper.getSessionFactory();
            Session session = sessionFactory.openSession();
            category = session.find(Category.class, id);
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return category;
    }

    /**
     * search for a list of categories
     * @param menuId the id of the menu for which we search categories
     * @return a list of categories for a menuId
     */
    public List<Category> findByMenuId(int menuId) {
        List<Category> allCategory;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            String string = "from Category c WHERE c.menuId = :menuId";
            Query query = session.createQuery(string);
            query.setParameter("menuId", menuId);
            allCategory = query.list();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return allCategory;
    }

    /**
     * deletes a category from the database
     * @param category the category we want to delete
     */
    public void delete(Category category) {
        Transaction transaction = null;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(category);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * adds a category from the database
     * @param category the category we want to add
     */
    public void save(Category category) {
        Transaction transaction = null;
        try {
            Session session = DBHelper.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(category);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
