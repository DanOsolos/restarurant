package utils;

import model.Category;
import model.Menu;
import model.Product;
import model.User;

import java.util.List;

public class Utils {
    /** Converts the price to String and formats it as a decimal number
     * @param price the long formatted price
     * @return the appropriately formatted price
     */
    public static String formatPrice(long price) {
        String tempPrice = String.valueOf(price);
        return tempPrice.substring(0, tempPrice.length() - 2) + "." + tempPrice.substring(tempPrice.length() - 2);
    }

    /**
     * Displays a nicely formatted menu using a certain width
     * @param menu the menu we want to display
     * @param numberOfCharacters the specified width
     */
    public static void displayMenu(Menu menu, int numberOfCharacters) {
        StringBuilder lines = new StringBuilder();
        for (int i = 0; i <= numberOfCharacters; i++) {
            lines.append("-");
        }
        for (Category category : menu.getCategoryList()) {
            StringBuilder blankString = new StringBuilder();
            int categoryNameLength = category.getName().length();
            int spaces = (numberOfCharacters - categoryNameLength) / 2;
            for (int i = 0; i <= spaces; i++) {
                blankString.insert(0, "=");
            }
            System.out.println(blankString + category.getName().toUpperCase() +
                    (categoryNameLength % 2 == 0 ? blankString.substring(1) : blankString));

            System.out.println(lines);
            for (Product product : category.getProducts()) {
                String line = product.getName();
                String formattedPrice = Utils.formatPrice(product.getPrice());
                for (int i = 0; i <= numberOfCharacters - product.getName().length() - formattedPrice.length(); i++) {
                    line = line + ".";
                }
                line = line + formattedPrice;
                System.out.println(line);
            }
            System.out.println(lines);
        }
    }

    /**
     * Displays a list of users
     * @param userList the user list to display
     */
    public static void displayUsers(List<User> userList) {
        for (User user : userList) {
            if (Constants.USERTYPE_ADMIN == user.getUserType())
                System.out.println(user.getUsername() + " - " + "administrator");
            else if (Constants.USERTYPE_WAITER == user.getUserType()) {
                System.out.println(user.getUsername() + " - " + "waiter");
            }
        }
    }
}
