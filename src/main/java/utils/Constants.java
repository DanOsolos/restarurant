package utils;

/**
 * General app constants
 */
public class Constants {
    public static final int USERTYPE_ADMIN = 1;
    public static final int USERTYPE_WAITER = 0;
}
