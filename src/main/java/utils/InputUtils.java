package utils;

import model.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class InputUtils {

    static final Scanner scanner = new Scanner(System.in);

    /**
     * @param categories list of categories to chose from
     * @return a category from a list of categories by user choice
     */
    public static Category getValidCategory(List<Category> categories) {
        for (int i = 0; i < categories.size(); i++) {
            System.out.println(i + 1 + ". " + categories.get(i).getName());
        }
        System.out.println("Chose an option");

        int option = scanner.nextInt();

        if (option < 1 || option > categories.size()) {
            System.out.println("Chose a valid option");
            return getValidCategory(categories);
        }
        return categories.get(option - 1);
    }

    /**
     * @param products list of products to chose from
     * @return a product from a list of products by user choice
     */
    public static Product getValidProduct(List<Product> products) {
        for (int i = 0; i < products.size(); i++) {
            System.out.println(i + 1 + ". " + products.get(i).getName());
        }
        System.out.println("Chose an option");

        int option = scanner.nextInt();

        if (option < 1 || option > products.size()) {
            System.out.println("Chose a valid option");
            return getValidProduct(products);
        }
        return products.get(option - 1);
    }

    /**
     * @param menus list of menus to chose from
     * @return a menu from a list of menus by user choice
     */
    public static Menu getValidMenu(List<Menu> menus) {
        for (int i = 0; i < menus.size(); i++) {
            System.out.println(i + 1 + ". " + menus.get(i).getTitle());
        }
        System.out.println("Chose an option");

        int option = scanner.nextInt();

        if (option < 1 || option > menus.size()) {
            System.out.println("Chose a valid option");
            return getValidMenu(menus);
        }
        return menus.get(option - 1);
    }

    /**
     * @param users list of users to chose from
     * @return a user from a list of users by user choice
     */
    public static User getValidUser(List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            System.out.println(i + 1 + ". " + users.get(i).getUsername());
        }
        System.out.println("Chose an option");

        int option = scanner.nextInt();

        if (option < 1 || option > users.size()) {
            System.out.println("Chose a valid option");
            return getValidUser(users);
        }
        return users.get(option - 1);
    }

    /**
     * Shows a message in the user console, reads a String user input and returns it
     * @param message a message to show
     * @return returns the value introduced by the user
     */
    public static String getString(String message) {
        System.out.println(message);
        scanner.nextLine();
        return scanner.nextLine();
    }
    /**
     * Fixes a input console bug
     * Shows a message in the user console, reads a String user input and returns it
     * @param message a message to show
     * @return returns the value introduced by the user
     */
    public static String getPassword(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

    /**
     * Shows a message in the user console, reads a long user input and returns it
     * @param message a message to show
     * @return returns the value introduced by the user
     */
    public static long getLong(String message) {
        System.out.println(message);
        return Long.parseLong(scanner.next());
    }

    /**
     * Shows a message in the user console, reads a char user input and returns it
     * @param message a message to show
     * @return returns the value introduced by the user
     */
    public static char getChar(String message) {
        System.out.println(message);
        return scanner.next().toUpperCase().charAt(0);
    }

    /**
     * Shows an admin menu and accepts valid user input
     */
    public static void adminMenu() {
        int option;
        do {
            System.out.println("Chose an option");
            System.out.println("1. Add product");
            System.out.println("2. Remove product");
            System.out.println("3. Add category");
            System.out.println("4. Remove category");
            System.out.println("5. Show menu");
            System.out.println("6. Order products");
            System.out.println("7. Create user");
            System.out.println("8. Remove user");
            System.out.println("9. Display users");
            System.out.println("0. Close");
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    String productName = InputUtils.getString("Enter a product name");
                    long price = InputUtils.getLong("Enter a new price");
                    System.out.println("Enter a category for the product");
                    int categoryId = InputUtils.getValidCategory(DataSingleton.getInstance().categoryRepository.findAll()).getCategoryId();
                    Product product = new Product(productName, price, categoryId);
                    DataSingleton.getInstance().productRepository.save(product);
                    break;
                case 2:
                    Product product1 = InputUtils.getValidProduct(DataSingleton.getInstance().productRepository.findAll());
                    DataSingleton.getInstance().productRepository.delete(product1);
                    break;
                case 3:
                    String categoryName = InputUtils.getString("Enter a new category");
                    int menuId = InputUtils.getValidMenu(DataSingleton.getInstance().menuRepository.findAll()).getMenuId();
                    Category category = new Category(categoryName, menuId);
                    DataSingleton.getInstance().categoryRepository.save(category);
                    break;
                case 4:
                    Category category1 = InputUtils.getValidCategory(DataSingleton.getInstance().categoryRepository.findAll());
                    if (DataSingleton.getInstance().productRepository.findByCategoryId(category1.getCategoryId()).isEmpty()) {
                        DataSingleton.getInstance().categoryRepository.delete(category1);
                    } else {
                        System.out.println("You can't delete a non-empty category");
                    }
                    break;
                case 5:
                    DataSingleton.getInstance().loadMenus();
                    for (Menu menu : DataSingleton.getInstance().getMenuList()) {
                        Utils.displayMenu(menu, 50);
                    }
                    break;
                case 6:
                    Map<Integer, Integer> productsToOrder = new HashMap<>();
                    char productOption = 'Y';
                    do {
                        categoryId = InputUtils.getValidCategory(DataSingleton.getInstance().categoryRepository.findAll()).getCategoryId();
                        int productId = InputUtils.getValidProduct(DataSingleton.getInstance().productRepository.findByCategoryId(categoryId)).getProductId();
                        int quantity = (int) (InputUtils.getLong("Enter a quantity for the selected product"));
                        productsToOrder.merge(productId, quantity, Integer::sum);
                        productOption = InputUtils.getChar("Do you want to continue? Y/N");
                    } while (productOption == 'Y');
                    for (Integer key : productsToOrder.keySet()) {
                        System.out.println(key + " " + productsToOrder.get(key));
                    }
                    break;
                case 7:
                    String userName = InputUtils.getString("Enter a new username");
                    String password = InputUtils.getPassword("Enter a new password");
                    int userType = (int) InputUtils.getLong("0. Waiter\n1. Admin");
                    User user = new User(userName,PasswordUtils.hashPassword(password),userType);
                    DataSingleton.getInstance().userRepository.save(user);
                    break;
                case 8:
                    user = InputUtils.getValidUser(DataSingleton.getInstance().userRepository.findAll());
                    DataSingleton.getInstance().userRepository.delete(user);
                    break;
                case 9:
                    Utils.displayUsers(DataSingleton.getInstance().userRepository.findAll());
                    break;
                case 0:
                    System.out.println("Program terminated");
                    break;
                default:
                    System.out.println("Enter a valid option");
                    break;
            }
        } while (option != 0);

    }

    /**
     * Shows an user (waiter) menu and accepts valid user input
     */
    public static void waiterMenu() {
        int option;
        do {
            System.out.println("Chose an option");
            System.out.println("1. Show menu");
            System.out.println("2. Order products");
            System.out.println("3. Close");
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    DataSingleton.getInstance().loadMenus();
                    for (Menu menu : DataSingleton.getInstance().getMenuList()) {
                        Utils.displayMenu(menu, 50);
                    }
                    break;
                case 2:
                    Map<Integer, Integer> productsToOrder = new HashMap<>();
                    char productOption = 'Y';
                    do {
                        int categoryId = InputUtils.getValidCategory(DataSingleton.getInstance().categoryRepository.findAll()).getCategoryId();
                        int productId = InputUtils.getValidProduct(DataSingleton.getInstance().productRepository.findByCategoryId(categoryId)).getProductId();
                        int quantity = (int) (InputUtils.getLong("Enter a quantity for the selected product"));
                        productsToOrder.merge(productId, quantity, Integer::sum);
                        productOption = InputUtils.getChar("Do you want to continue? Y/N");
                    } while (productOption == 'Y');
                    for (Integer key : productsToOrder.keySet()) {
                        System.out.println(key + " " + productsToOrder.get(key));
                    }
                    break;
                case 3:
                    System.out.println("Program terminated");
                    break;
                default:
                    System.out.println("Enter a valid option");
                    break;
            }
        } while (option != 3);

    }

    /**
     * Reads a user passowerd and looks up the user in the database
     * @return the user found
     */
    public static User userLogin() {
        do {
            String plainTextPassword = InputUtils.getPassword("Please enter your password");
            List<User> userList = DataSingleton.getInstance().userRepository.findAll();

            for (User user : userList) {
                if (PasswordUtils.checkPassword(plainTextPassword, user.getPassword())) {
                    return user;
                }
            }
        } while (true);
    }
}
