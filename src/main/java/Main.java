import db.DBHelper;
import utils.Constants;
import utils.InputUtils;

public class Main {
    public static void main(String[] args) {
        DBHelper.getSessionFactory();
        int userType = InputUtils.userLogin().getUserType();

        switch (userType){
            case Constants.USERTYPE_ADMIN:
                InputUtils.adminMenu();
                break;
            case Constants.USERTYPE_WAITER:
                InputUtils.waiterMenu();
                break;
        }
        DBHelper.shutDown();
//        Menu menu = new Menu("Meniu");
//        Category category1 = new Category("Bauturi");
//        Category category2 = new Category("Pizza");
//        Product product1 = new Product("Cola", 1000);
//        Product product2 = new Product( "Pizza capriciosa", 4030);
//        category1.addItemsToMenu(product1);
//        category2.addItemsToMenu(product2);
//        menu.addCategory(category1);
//        menu.addCategory(category2);
//
//        Utils.displayMenu(menu, 50);


//        System.out.println(RepositoriesSingleton.getInstance().menuRepository.findById(1));
//        System.out.println(RepositoriesSingleton.getInstance().menuRepository.findById(2));


//        Category category = new Category("Deserturi", 2);
//        RepositoriesSingleton.getInstance().categoryRepository.save(category);
//        System.out.println(RepositoriesSingleton.getInstance().categoryRepository.findAll());

//        Product product = new Product("Tiramisu", 2000, 4);
//        RepositoriesSingleton.getInstance().productRepository.save(product);
//        System.out.println(RepositoriesSingleton.getInstance().productRepository.findAll());
//        List<Menu> allMenus = DataSingleton.getInstance().menuRepository.findAll();


//        System.out.println(RepositoriesSingleton.getInstance().productRepository.findByCategoryId(4));
//        System.out.println(RepositoriesSingleton.getInstance().categoryRepository.findByMenuId(2))

//        User user1 = new User("Ion", PasswordUtils.hashPassword("1234"),1);
//        User user2 = new User("Andrei", PasswordUtils.hashPassword("12345"),0);
//        DataSingleton.getInstance().userRepository.save(user1);
//        DataSingleton.getInstance().userRepository.save(user2);

//        User user1 = new User("Ana", PasswordUtils.hashPassword("1234"),1);
//        DataSingleton.getInstance().userRepository.save(user1);

//        menuRepository.save(menu);
//        categoryRepository.save(category1);
//        categoryRepository.save(category2);
//        productRepository.save(product1);
//        productRepository.save(product2);
    }
}
